package ie.ajax.animationtest;

import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView ivVector;
    private AnimatedVectorDrawable avVector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivVector = (ImageView) findViewById(R.id.ivVector);
        avVector = (AnimatedVectorDrawable) ivVector.getDrawable();

        ivVector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                avVector.start();
            }
        });
    }
}
