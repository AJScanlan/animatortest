package ie.ajax.animationtest;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;

/**
 * @author Alexander Scanlan
 * @since 17/10/2016
 */

public class FloatingArrowButton extends FloatingActionButton {


    public FloatingArrowButton(Context context) {
        super(context);
    }

    public FloatingArrowButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloatingArrowButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
